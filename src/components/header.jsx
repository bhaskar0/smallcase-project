import React from "react";
import { Tabs, TabList, Tab, Button } from "@chakra-ui/core";

export default function Header() {
  return (
    <Tabs>
      <TabList display="grid" gridTemplateColumns="1fr 1fr 1fr">
        <h2> Small Case</h2>
        <div style={{ display: "flex" }}>
          {" "}
          <Tab>Discover</Tab>
          <Tab>Creators</Tab>
        </div>

        <div style={{ display: "flex" }}>
          <ul>
            <a href="https://smallcase.freshteam.com/jobs">we're hiring</a>{" "}
            &nbsp;&nbsp;
            <a href="https://blog.smallcase.com/">Blog</a>&nbsp;&nbsp;
            <a href="https://www.smallcase.com/signup">Sign Up</a>&nbsp;&nbsp;
          </ul>
          <Button variantColor="teal" variant="outline">
            Login
          </Button>
        </div>
      </TabList>
    </Tabs>
  );
}
